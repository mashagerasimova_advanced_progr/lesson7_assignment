﻿namespace HW7_MashaGerasimova
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.txtYourScore = new System.Windows.Forms.Label();
            this.txtOppScore = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // txtYourScore
            // 
            this.txtYourScore.AutoSize = true;
            this.txtYourScore.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtYourScore.Location = new System.Drawing.Point(30, 30);
            this.txtYourScore.Name = "txtYourScore";
            this.txtYourScore.Size = new System.Drawing.Size(70, 13);
            this.txtYourScore.TabIndex = 1;
            this.txtYourScore.Text = "Your score: 0";
            // 
            // txtOppScore
            // 
            this.txtOppScore.AutoSize = true;
            this.txtOppScore.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.txtOppScore.Location = new System.Drawing.Point(1240, 30);
            this.txtOppScore.Name = "txtOppScore";
            this.txtOppScore.Size = new System.Drawing.Size(95, 13);
            this.txtOppScore.TabIndex = 2;
            this.txtOppScore.Text = "Opponent score: 0";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1299, 682);
            this.Controls.Add(this.txtOppScore);
            this.Controls.Add(this.txtYourScore);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label txtYourScore;
        private System.Windows.Forms.Label txtOppScore;
    }
}

