﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Sockets;


namespace HW7_MashaGerasimova
{
    public partial class Form1 : Form
    {
        const int amount = 10;
        const int cardWidth = 75;
        const int cardHeight = 120;
        const int redStartX = 80;
        const int redY = 500;
        const int redNext = 50;
        const int blueX = 630;
        const int blueY = 100;
        const int exitY = 30;
        const int types = 4;
        const int characters = 14;

        const int ace = 1;
        const int two = 2;
        const int three = 3;
        const int four = 4;
        const int five = 5;
        const int six = 6;
        const int seven = 7;
        const int eight = 8;
        const int nine = 9;
        const int ten = 10;
        const int jack = 11;
        const int queen = 12;
        const int king = 13;

        const string startGame = "0";
        const string cardCode = "1";
        const string endGame = "2";
        const string notCardCode = "000";
        const string addToCardCode = "0";

        const string hearts = "H";
        const string diamonds = "D";
        const string clubs = "C";
        const string spades = "S";
        const int typeInd = 3;
        

        const int port = 8820;
        const int buff = 4;

        string send = "";

        Button exitGame;
        System.Windows.Forms.PictureBox oppCard;
        Image lotteriedCard;
        Image oppCardImg;

        System.Net.Sockets.TcpClient clientStream = new System.Net.Sockets.TcpClient();

        NetworkStream serverStream;

        string serverMsg = "";

        Thread mainT;

        int myScore = 0;
        int oppScore = 0;

        public Form1()
        {
            InitializeComponent();  
        }

        private void Form1_Load(object sender, EventArgs e) // Initiates the game
        {
            InitGame(); // Blue card, 'exit' button, scores 
            clientStream.Connect("127.0.0.1", port); // Connecting to the server
            mainT = new Thread(InitServer); // Thread
            mainT.Start();
        }

        private void GenerateCards() // Generates 10 red cards
        {
            int i = 0;
            Point location = new Point(redStartX, redY); // First card's location
            for (i=0; i<amount; i++) // 10 times - 10 cards
            {
                System.Windows.Forms.PictureBox currPic = new PictureBox();
                currPic.Name = "card" + i; // Name
                currPic.Image = global::HW7_MashaGerasimova.Properties.Resources.card_back_red; // Red card
                currPic.Location = location; // Location
                currPic.Size = new System.Drawing.Size(cardWidth, cardHeight); // Image size
                currPic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage; // Format
                currPic.Click += delegate (object sender, EventArgs e) // Click event on a card
                                    {
                                        send = "";
                                        send += cardCode; // Starting to build a message to the server
                                        Random random = new Random();
                                        int type = random.Next(0, types); // Type lottery
                                        int character = random.Next(1, characters); // Character lottery
                                        BuildMsg(type, character); // Finishing building a message to server and updating the card
                                        currPic.Image = lotteriedCard; // Updating the image
                                        Invoke((MethodInvoker)delegate { this.Controls.Add(currPic); }); // Updating the graffic
                                        string exit = endGame + notCardCode; // "2000"
                                        if (!serverMsg.Equals(exit)) // While the server's message is not "2000"
                                        {
                                            SendMsg(serverStream, send); // Sending info about my card to the server
                                            serverMsg = GetMsg(serverStream); // Getting server's message
                                            Decode(serverMsg); // Decoding the server's message about the opponent's card
                                            oppCard.Image = oppCardImg; // Updating opponent's card
                                            Invoke((MethodInvoker)delegate { this.Controls.Add(oppCard); }); // Updating the graffic
                                            if (CheckCards(send, serverMsg)!=0) // If the result is 0 there is no updates in scores
                                            {
                                                if (CheckCards(send, serverMsg)==1) // My card is smaller
                                                {
                                                    oppScore++; // Increasing the score
                                                    txtOppScore.Text = "Opponent score: " + oppScore; // Updating the score
                                                }
                                                else // My card is bigger
                                                {
                                                    myScore++; // Increasing the score
                                                    txtYourScore.Text = "Your score: " + myScore;  // Updating the score      
                                                }
                                            }
                                            send = "";
                                        }

                                    };
                Invoke((MethodInvoker) delegate { this.Controls.Add(currPic); }); // Updating the graffic
                location.X = location.X + currPic.Size.Width + redNext; // Updating the next location
            }
        }

        private int CheckCards(string myCard, string oppCard) // Comparing two cards
        {
            int ret = 0;
            if (myCard.Equals(oppCard)) // Same cards
            {
                ret = 0;
            }
            else
            {
                string myNum = myCard[1].ToString() + myCard[1 + 1].ToString(); // Getting my number
                string oppNum = oppCard[1].ToString() + oppCard[1 + 1].ToString(); // Getting opponent's number
                if (Int32.Parse(myNum) < Int32.Parse(oppNum)) // My number is smaller than the opponent's one
                {
                    ret = 1;
                }
                else if (Int32.Parse(myNum) > Int32.Parse(oppNum)) // My number is bigger than the opponent's one
                {
                    ret = 1 + 1;
                }                     
            }
            return ret;
        }

       

        private void SendMsg(NetworkStream serverStream, string msg) // Sending msg to server
        {
            byte[] buffer = new ASCIIEncoding().GetBytes(msg);
            serverStream.Write(buffer, 0, buff);
            serverStream.Flush();
        }

        private string GetMsg(NetworkStream serverStream) // Getting msg from server
        {
            byte[] bufferIn = new byte[4];
            int bytesRead = serverStream.Read(bufferIn, 0, buff);
            string input = new ASCIIEncoding().GetString(bufferIn);
            return input;
        }

        private void button1_Click(object sender, EventArgs e)
        {
                    
        }

        private void InitServer() // Initializing the server connection
        {
            serverStream = clientStream.GetStream();

            string serverMsg = GetMsg(serverStream); // First msg from server - "0000"

            if (serverMsg.Equals(startGame + notCardCode)) // If the message is "0000"
            {
                GenerateCards(); // Generating 10 red cards
                Invoke((MethodInvoker)delegate // Making the buttons enable
                {
                    oppCard.Enabled = true;
                    exitGame.Enabled = true;
                }); 
            }
        }

        private void InitGame() // Initializing the game
        {
            oppCard = new PictureBox(); // Blue card
            oppCard.Name = "opponent"; // Name
            oppCard.Image = global::HW7_MashaGerasimova.Properties.Resources.card_back_blue; // Image
            oppCard.Location = new Point(blueX, blueY); // Location
            oppCard.Size = new System.Drawing.Size(cardWidth, cardHeight); // Image size
            oppCard.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage; // Format  
            this.Controls.Add(oppCard); // Updating the graffic

            exitGame = new Button(); // 'Exit' button
            exitGame.Name = "exit"; // Name
            exitGame.Text = "Exit game"; // Content
            exitGame.Location = new Point(blueX, exitY); // Location
            exitGame.Click += delegate (object sender2, EventArgs e2) // Click event
            {
                this.Close();
                Application.Exit();
            };
            this.Controls.Add(exitGame); // Updating the graffic
            // Making buttons disenabled
            oppCard.Enabled = false; 
            exitGame.Enabled = false;
        }

        private void BuildMsg(int type, int character) // Builds a messege to server and updating the card
        {
            if (character < 10) // One digit in the number
            {
                send += addToCardCode + character.ToString(); // Adding '0' before the digit
            }
            else
            {
                send += character.ToString(); // Regularly
            }

            if (type == 0) // Hearts
            {
                send += hearts; // Finishing with the message

                if (character == ace)
                {
                    lotteriedCard = global::HW7_MashaGerasimova.Properties.Resources.ace_of_hearts;
                }
                else if (character == two)
                {
                    lotteriedCard = global::HW7_MashaGerasimova.Properties.Resources._2_of_hearts;
                }
                else if (character == three)
                {
                    lotteriedCard = global::HW7_MashaGerasimova.Properties.Resources._3_of_hearts;
                }
                else if (character == four)
                {
                    lotteriedCard = global::HW7_MashaGerasimova.Properties.Resources._4_of_hearts;
                }
                else if (character == five)
                {
                    lotteriedCard = global::HW7_MashaGerasimova.Properties.Resources._5_of_hearts;
                }
                else if (character == six)
                {
                    lotteriedCard = global::HW7_MashaGerasimova.Properties.Resources._6_of_hearts;
                }
                else if (character == seven)
                {
                    lotteriedCard = global::HW7_MashaGerasimova.Properties.Resources._7_of_hearts;
                }
                else if (character == eight)
                {
                    lotteriedCard = global::HW7_MashaGerasimova.Properties.Resources._8_of_hearts;
                }
                else if (character == nine)
                {
                    lotteriedCard = global::HW7_MashaGerasimova.Properties.Resources._9_of_hearts;
                }
                else if (character == ten)
                {
                    lotteriedCard = global::HW7_MashaGerasimova.Properties.Resources._10_of_hearts;
                }
                else if (character == jack)
                {
                    lotteriedCard = global::HW7_MashaGerasimova.Properties.Resources.jack_of_hearts2;
                }
                else if (character == queen)
                {
                    lotteriedCard = global::HW7_MashaGerasimova.Properties.Resources.queen_of_hearts2;
                }
                else if (character == king)
                {
                    lotteriedCard = global::HW7_MashaGerasimova.Properties.Resources.king_of_hearts2;
                }
            } 
            else if (type == 1) // Diamonds
            {
                send += diamonds; // Finishing with the message

                if (character == ace)
                {
                    lotteriedCard = global::HW7_MashaGerasimova.Properties.Resources.ace_of_diamonds;
                }
                else if (character == two)
                {
                    lotteriedCard = global::HW7_MashaGerasimova.Properties.Resources._2_of_diamonds;
                }
                else if (character == three)
                {
                    lotteriedCard = global::HW7_MashaGerasimova.Properties.Resources._3_of_diamonds;
                }
                else if (character == four)
                {
                    lotteriedCard = global::HW7_MashaGerasimova.Properties.Resources._4_of_diamonds;
                }
                else if (character == five)
                {
                    lotteriedCard = global::HW7_MashaGerasimova.Properties.Resources._5_of_diamonds;
                }
                else if (character == six)
                {
                    lotteriedCard = global::HW7_MashaGerasimova.Properties.Resources._6_of_diamonds;
                }
                else if (character == seven)
                {
                    lotteriedCard = global::HW7_MashaGerasimova.Properties.Resources._7_of_diamonds;
                }
                else if (character == eight)
                {
                    lotteriedCard = global::HW7_MashaGerasimova.Properties.Resources._8_of_diamonds;
                }
                else if (character == nine)
                {
                    lotteriedCard = global::HW7_MashaGerasimova.Properties.Resources._9_of_diamonds;
                }
                else if (character == ten)
                {
                    lotteriedCard = global::HW7_MashaGerasimova.Properties.Resources._10_of_diamonds;
                }
                else if (character == jack)
                {
                    lotteriedCard = global::HW7_MashaGerasimova.Properties.Resources.jack_of_diamonds2;
                }
                else if (character == queen)
                {
                    lotteriedCard = global::HW7_MashaGerasimova.Properties.Resources.queen_of_diamonds2;
                }
                else if (character == king)
                {
                    lotteriedCard = global::HW7_MashaGerasimova.Properties.Resources.king_of_diamonds2;
                }
            }
            else if (type == 2) // Clubs
            {
                send += clubs; // Finishing with the message

                if (character == ace)
                {
                    lotteriedCard = global::HW7_MashaGerasimova.Properties.Resources.ace_of_clubs;
                }
                else if (character == two)
                {
                    lotteriedCard = global::HW7_MashaGerasimova.Properties.Resources._2_of_clubs;
                }
                else if (character == three)
                {
                    lotteriedCard = global::HW7_MashaGerasimova.Properties.Resources._3_of_clubs;
                }
                else if (character == four)
                {
                    lotteriedCard = global::HW7_MashaGerasimova.Properties.Resources._4_of_clubs;
                }
                else if (character == five)
                {
                    lotteriedCard = global::HW7_MashaGerasimova.Properties.Resources._5_of_clubs;
                }
                else if (character == six)
                {
                    lotteriedCard = global::HW7_MashaGerasimova.Properties.Resources._6_of_clubs;
                }
                else if (character == seven)
                {
                    lotteriedCard = global::HW7_MashaGerasimova.Properties.Resources._7_of_clubs;
                }
                else if (character == eight)
                {
                    lotteriedCard = global::HW7_MashaGerasimova.Properties.Resources._8_of_clubs;
                }
                else if (character == nine)
                {
                    lotteriedCard = global::HW7_MashaGerasimova.Properties.Resources._9_of_clubs;
                }
                else if (character == ten)
                {
                    lotteriedCard = global::HW7_MashaGerasimova.Properties.Resources._10_of_clubs;
                }
                else if (character == jack)
                {
                    lotteriedCard = global::HW7_MashaGerasimova.Properties.Resources.jack_of_clubs2;
                }
                else if (character == queen)
                {
                    lotteriedCard = global::HW7_MashaGerasimova.Properties.Resources.queen_of_clubs2;
                }
                else if (character == king)
                {
                    lotteriedCard = global::HW7_MashaGerasimova.Properties.Resources.king_of_clubs2;
                }
            }
            else // Spades
            {
                send += spades; // Finishing with the message

                if (character == ace)
                {
                    lotteriedCard = global::HW7_MashaGerasimova.Properties.Resources.ace_of_spades2;
                }
                else if (character == two)
                {
                    lotteriedCard = global::HW7_MashaGerasimova.Properties.Resources._2_of_spades;
                }
                else if (character == three)
                {
                    lotteriedCard = global::HW7_MashaGerasimova.Properties.Resources._3_of_spades;
                }
                else if (character == four)
                {
                    lotteriedCard = global::HW7_MashaGerasimova.Properties.Resources._4_of_spades;
                }
                else if (character == five)
                {
                    lotteriedCard = global::HW7_MashaGerasimova.Properties.Resources._5_of_spades;
                }
                else if (character == six)
                {
                    lotteriedCard = global::HW7_MashaGerasimova.Properties.Resources._6_of_spades;
                }
                else if (character == seven)
                {
                    lotteriedCard = global::HW7_MashaGerasimova.Properties.Resources._7_of_spades;
                }
                else if (character == eight)
                {
                    lotteriedCard = global::HW7_MashaGerasimova.Properties.Resources._8_of_spades;
                }
                else if (character == nine)
                {
                    lotteriedCard = global::HW7_MashaGerasimova.Properties.Resources._9_of_spades;
                }
                else if (character == ten)
                {
                    lotteriedCard = global::HW7_MashaGerasimova.Properties.Resources._10_of_spades;
                }
                else if (character == jack)
                {
                    lotteriedCard = global::HW7_MashaGerasimova.Properties.Resources.jack_of_spades2;
                }
                else if (character == queen)
                {
                    lotteriedCard = global::HW7_MashaGerasimova.Properties.Resources.queen_of_spades2;
                }
                else if (character == king)
                {
                    lotteriedCard = global::HW7_MashaGerasimova.Properties.Resources.king_of_spades2;
                }
            }

        }

        private void Decode(string cardCode) // Decodes the message received from server
        {
            string num = cardCode[1].ToString() + cardCode[1 + 1].ToString(); // Card number(character)
            string type = cardCode[typeInd].ToString(); // Card type
            if (type.Equals(hearts)) // Hearts
            {
                if (Int32.Parse(num) == ace)
                {
                    oppCardImg = global::HW7_MashaGerasimova.Properties.Resources.ace_of_hearts;
                }
                else if (Int32.Parse(num) == two)
                {
                    oppCardImg = global::HW7_MashaGerasimova.Properties.Resources._2_of_hearts;
                }
                else if (Int32.Parse(num) == three)
                {
                    oppCardImg = global::HW7_MashaGerasimova.Properties.Resources._3_of_hearts;
                }
                else if (Int32.Parse(num) == four)
                {
                    oppCardImg = global::HW7_MashaGerasimova.Properties.Resources._4_of_hearts;
                }
                else if (Int32.Parse(num) == five)
                {
                    oppCardImg = global::HW7_MashaGerasimova.Properties.Resources._5_of_hearts;
                }
                else if (Int32.Parse(num) == six)
                {
                    oppCardImg = global::HW7_MashaGerasimova.Properties.Resources._6_of_hearts;
                }
                else if (Int32.Parse(num) == seven)
                {
                    oppCardImg = global::HW7_MashaGerasimova.Properties.Resources._7_of_hearts;
                }
                else if (Int32.Parse(num) == eight)
                {
                    oppCardImg = global::HW7_MashaGerasimova.Properties.Resources._8_of_hearts;
                }
                else if (Int32.Parse(num) == nine)
                {
                    oppCardImg = global::HW7_MashaGerasimova.Properties.Resources._9_of_hearts;
                }
                else if (Int32.Parse(num) == ten)
                {
                    oppCardImg = global::HW7_MashaGerasimova.Properties.Resources._10_of_hearts;
                }
                else if (Int32.Parse(num) == jack)
                {
                    oppCardImg = global::HW7_MashaGerasimova.Properties.Resources.jack_of_hearts2;
                }
                else if (Int32.Parse(num) == queen)
                {
                    oppCardImg = global::HW7_MashaGerasimova.Properties.Resources.queen_of_hearts2;
                }
                else if (Int32.Parse(num) == king)
                {
                    oppCardImg = global::HW7_MashaGerasimova.Properties.Resources.king_of_hearts2;
                }
            }
            else if (type.Equals(diamonds)) // Diamonds
            {
                if (Int32.Parse(num) == ace)
                {
                    oppCardImg = global::HW7_MashaGerasimova.Properties.Resources.ace_of_diamonds;
                }
                else if (Int32.Parse(num) == two)
                {
                    oppCardImg = global::HW7_MashaGerasimova.Properties.Resources._2_of_diamonds;
                }
                else if (Int32.Parse(num) == three)
                {
                    oppCardImg = global::HW7_MashaGerasimova.Properties.Resources._3_of_diamonds;
                }
                else if (Int32.Parse(num) == four)
                {
                    oppCardImg = global::HW7_MashaGerasimova.Properties.Resources._4_of_diamonds;
                }
                else if (Int32.Parse(num) == five)
                {
                    oppCardImg = global::HW7_MashaGerasimova.Properties.Resources._5_of_diamonds;
                }
                else if (Int32.Parse(num) == six)
                {
                    oppCardImg = global::HW7_MashaGerasimova.Properties.Resources._6_of_diamonds;
                }
                else if (Int32.Parse(num) == seven)
                {
                    oppCardImg = global::HW7_MashaGerasimova.Properties.Resources._7_of_diamonds;
                }
                else if (Int32.Parse(num) == eight)
                {
                    oppCardImg = global::HW7_MashaGerasimova.Properties.Resources._8_of_diamonds;
                }
                else if (Int32.Parse(num) == nine)
                {
                    oppCardImg = global::HW7_MashaGerasimova.Properties.Resources._9_of_diamonds;
                }
                else if (Int32.Parse(num) == ten)
                {
                    oppCardImg = global::HW7_MashaGerasimova.Properties.Resources._10_of_diamonds;
                }
                else if (Int32.Parse(num) == jack)
                {
                    oppCardImg = global::HW7_MashaGerasimova.Properties.Resources.jack_of_diamonds2;
                }
                else if (Int32.Parse(num) == queen)
                {
                    oppCardImg = global::HW7_MashaGerasimova.Properties.Resources.queen_of_diamonds2;
                }
                else if (Int32.Parse(num) == king)
                {
                    oppCardImg = global::HW7_MashaGerasimova.Properties.Resources.king_of_diamonds2;
                }
            }
            else if (type.Equals(clubs)) // Clubs
            {
                if (Int32.Parse(num) == ace)
                {
                    oppCardImg = global::HW7_MashaGerasimova.Properties.Resources.ace_of_clubs;
                }
                else if (Int32.Parse(num) == two)
                {
                    oppCardImg = global::HW7_MashaGerasimova.Properties.Resources._2_of_clubs;
                }
                else if (Int32.Parse(num) == three)
                {
                    oppCardImg = global::HW7_MashaGerasimova.Properties.Resources._3_of_clubs;
                }
                else if (Int32.Parse(num) == four)
                {
                    oppCardImg = global::HW7_MashaGerasimova.Properties.Resources._4_of_clubs;
                }
                else if (Int32.Parse(num) == five)
                {
                    oppCardImg = global::HW7_MashaGerasimova.Properties.Resources._5_of_clubs;
                }
                else if (Int32.Parse(num) == six)
                {
                    oppCardImg = global::HW7_MashaGerasimova.Properties.Resources._6_of_clubs;
                }
                else if (Int32.Parse(num) == seven)
                {
                    oppCardImg = global::HW7_MashaGerasimova.Properties.Resources._7_of_clubs;
                }
                else if (Int32.Parse(num) == eight)
                {
                    oppCardImg = global::HW7_MashaGerasimova.Properties.Resources._8_of_clubs;
                }
                else if (Int32.Parse(num) == nine)
                {
                    oppCardImg = global::HW7_MashaGerasimova.Properties.Resources._9_of_clubs;
                }
                else if (Int32.Parse(num) == ten)
                {
                    oppCardImg = global::HW7_MashaGerasimova.Properties.Resources._10_of_clubs;
                }
                else if (Int32.Parse(num) == jack)
                {
                    oppCardImg = global::HW7_MashaGerasimova.Properties.Resources.jack_of_clubs2;
                }
                else if (Int32.Parse(num) == queen)
                {
                    oppCardImg = global::HW7_MashaGerasimova.Properties.Resources.queen_of_clubs2;
                }
                else if (Int32.Parse(num) == king)
                {
                    oppCardImg = global::HW7_MashaGerasimova.Properties.Resources.king_of_clubs2;
                }
            }
            else if (type.Equals(spades)) // Spades
            {
                if (Int32.Parse(num) == ace)
                {
                    oppCardImg = global::HW7_MashaGerasimova.Properties.Resources.ace_of_spades2;
                }
                else if (Int32.Parse(num) == two)
                {
                    oppCardImg = global::HW7_MashaGerasimova.Properties.Resources._2_of_spades;
                }
                else if (Int32.Parse(num) == three)
                {
                    oppCardImg = global::HW7_MashaGerasimova.Properties.Resources._3_of_spades;
                }
                else if (Int32.Parse(num) == four)
                {
                    oppCardImg = global::HW7_MashaGerasimova.Properties.Resources._4_of_spades;
                }
                else if (Int32.Parse(num) == five)
                {
                    oppCardImg = global::HW7_MashaGerasimova.Properties.Resources._5_of_spades;
                }
                else if (Int32.Parse(num) == six)
                {
                    oppCardImg = global::HW7_MashaGerasimova.Properties.Resources._6_of_spades;
                }
                else if (Int32.Parse(num) == seven)
                {
                    oppCardImg = global::HW7_MashaGerasimova.Properties.Resources._7_of_spades;
                }
                else if (Int32.Parse(num) == eight)
                {
                    oppCardImg = global::HW7_MashaGerasimova.Properties.Resources._8_of_spades;
                }
                else if (Int32.Parse(num) == nine)
                {
                    oppCardImg = global::HW7_MashaGerasimova.Properties.Resources._9_of_spades;
                }
                else if (Int32.Parse(num) == ten)
                {
                    oppCardImg = global::HW7_MashaGerasimova.Properties.Resources._10_of_spades;
                }
                else if (Int32.Parse(num) == jack)
                {
                    oppCardImg = global::HW7_MashaGerasimova.Properties.Resources.jack_of_spades2;
                }
                else if (Int32.Parse(num) == queen)
                {
                    oppCardImg = global::HW7_MashaGerasimova.Properties.Resources.queen_of_spades2;
                }
                else if (Int32.Parse(num) == king)
                {
                    oppCardImg = global::HW7_MashaGerasimova.Properties.Resources.king_of_spades2;
                }
            }
        }
    }
}
